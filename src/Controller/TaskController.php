<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\TaskCreator;
use App\Service\TaskEditor;
use App\Provider\TaskProvider;

class TaskController extends AbstractController
{

    /**
     * @Route("/tasks", methods={"GET"}, name="task_list")
     */
    public function list(Request $request, TaskProvider $provider)
    {
        $searchParams = $request->query->all();
        $tasks = $provider->search($searchParams, true);
        return $this->json([
            'tasks' => $tasks
        ]);
    }

    /**
     * @Route("/task/{id}", methods={"GET"}, name="task_single")
     */
    public function single($id, TaskProvider $provider)
    {
        $task = $provider->single($id, true);
        return $this->json([
            'task' => $task
        ]);
    }

    /**
     * @Route("/task/{id}/start", methods={"PUT"}, name="task_start")
     */
    public function start($id, TaskEditor $taskEditor, TaskProvider $provider)
    {
        $task = $provider->single($id);
        $result = $taskEditor->enable($task, true);
        return $this->json([
            'result' => $result
        ]);
    }

    /**
     * @Route("/task/{id}/stop", methods={"PUT"}, name="task_stop")
     */
    public function stop($id, TaskEditor $taskEditor, TaskProvider $provider)
    {
        $task = $provider->single($id);
        $result = $taskEditor->enable($task, false);
        return $this->json([
            'result' => $result
        ]);
    }

    /**
     * @Route("/task/{id}", methods={"PUT"}, name="task_edit")
     */
    public function edit($id, TaskEditor $taskEditor, TaskProvider $provider, Request $request)
    {
        $params = [];
        if ($request->getContentType() === 'json') {
            if ($content = $request->getContent()) {
                $params = json_decode($content, true);
            }
        };
        $task = $provider->single($id);
        $result = $taskEditor->edit($task, $params);
        $model = $provider->single($id, true);
        return $this->json([
            'task' => $model
        ]);
    }

    /**
     * @Route("/task", methods={"POST"}, name="task_create")
     */
    public function create(Request $request, TaskCreator $taskCreator, TaskProvider $provider)
    {
        $params = [];
        if ($request->getContentType() === 'json') {
            if ($content = $request->getContent()) {
                $params = json_decode($content, true);
            }
        };
        $task = $taskCreator->create($params);
        $model = $provider->single($task->getId(), true);
        return $this->json([
            'task_id' => $task->getId(),
            'task' => $model
        ]);
    }
}
