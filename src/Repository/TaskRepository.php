<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function save(Task $task){

    }

    public function findOne($id){
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.id > :$id')
            //->setParameter('price', $price)
            //->orderBy('p.price', 'ASC')
            ->getQuery();
        $qb->setMaxResults(1);
       // $result = $qb->getOneOrNullResult();
        return [];
    }

    public function search($params = []): array
    {
        $query = $this->createQueryBuilder('t')
            //->andWhere('t.exampleField = :val')
            //->setParameter('val', $value)
            //->orderBy('t.id', 'ASC')
            //->setMaxResults(10)
        ;
        if( count($params) > 0 ){
            foreach($params  as $key => $value ){
                switch($key){
                    case 'id':
                        $query ->andWhere('t.id = :id') ->setParameter('id', $value);
                        break;
                    case 'orderBy':
                        //$query->orderBy("t.$value", 'ASC');
                        $query->orderBy("t.deadline", 'DESC');
                        break;
                    case 'orderDesc':
                        //$task->setDescription($value);
                        break;
                    case 'description':
                        $query ->andWhere('t.description = :description') ->setParameter('description', $value);
                        break;
                    case 'category':
                        $query ->andWhere('t.category = :category') ->setParameter('category', $value);
                        break;
                    case 'priority':
                        $query ->andWhere('t.priority = :priority') ->setParameter('priority', $value);
                        break;
                    case 'comments':
                        $query ->andWhere('t.comments = :comments') ->setParameter('comments', $value);
                        break;
                    case 'isEnable':
                        $query ->andWhere('t.isEnable = :isEnable') ->setParameter('isEnable', $value);
                        break;
                    case 'userFirstname':
                        $query ->andWhere('t.userFirstname = :userFirstname') ->setParameter('userFirstname', $value);
                        break;
                    case 'userSurname':
                        $query ->andWhere('t.userSurname = :userSurname') ->setParameter('userSurname', $value);
                        break;
                    case 'userMail':
                        $query ->andWhere('t.userMail = :userMail') ->setParameter('userMail', $value);
                        break;
                }
            }
        }
        $results = $query->getQuery()->getResult();
        return $results;
    }

    // /**
    //  * @return Task[] Returns an array of Task objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Task
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
