<?php

namespace App\Service;

use App\Entity\Task;

class TaskValidator{

    public function valid($userParams = []){
        $params = [
            'id' => null,
            'orderBy' => null,
            'subject' => null,
            'description' => null,
            'category' => null,
            'priority' => null, //(INFO, WARNING, ERROR)
            'comments' => null,
            'isEnable' => false,
            'deadline' => null,
            'userFirstname' => null,
            'userSurname'=> null,
            'userMail' => null,
        ];
        if( $userParams && count($userParams) > 0 ){
            foreach($userParams as $key => $value){
                if( $params[$key] === null ){
                    $params[$key] = $value;
                }
            }
        }
        return $params;
    }

}
