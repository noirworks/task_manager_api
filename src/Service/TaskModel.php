<?php

namespace App\Service;

use App\Entity\Task;

class TaskModel{

    public $id;
    public $subject;
    public $description;
    public $category;
    public $priority;
    public $comments;
    public $isEnable;
    public $userFirstname;
    public $userSurname;
    public $userMail;
    public $deadline;

    public function __construct($params = null){
        if( is_object($params) ){
            $this->makeFromObject($params);
        }else if( is_array($params) ){
            $this->makeFromArray($params);
        }
    }

    public function makeFromObject(Task $task){
        $this->id = $task->getId();
        $this->subject = $task->getSubject();
        $this->description = $task->getDescription();
        $this->category = $task->getCategory();
        $this->priority = $task->getPriority();
        $this->comments = $task->getComments();
        $this->isEnable = $task->getIsEnable();
        $this->deadline = $task->getDeadline();
        $this->userFirstname = $task->getUserFirstname();
        $this->userSurname = $task->getUserSurname();
        $this->userMail = $task->getUserMail();
    }

    public function makeFromArray(Array $params = []){
        if( count($params) > 0 ){
            foreach($params  as $key => $value ){
                switch($key){
                    case 'id':
                        $this->id = $value;
                        break;
                    case 'subject':
                        $this->subject = $value;
                        break;
                    case 'description':
                        $this->description = $value;
                        break;
                    case 'deadline':
                        $this->deadline = $value;
                        break;
                    case 'category':
                        $this->category = $value;
                        break;
                    case 'priority':
                        $this->priority = $value;
                        break;
                    case 'comments':
                        $this->comments = $value;
                        break;
                    case 'isEnable':
                        $this->isEnable = $value;
                        break;
                    case 'userFirstname':
                        $this->userFirstname = $value;
                        break;
                    case 'userSurname':
                        $this->userSurname = $value;
                        break;
                    case 'userMail':
                        $this->userMail = $value;
                        break;
                }
            }
        }
    }

    public function getArray(){
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'category' => $this->category,
            'priority' => $this->priority,
            'comments' => $this->comments,
            'deadline' => $this->deadline,
            'isEnable' => $this->isEnable,
            'userFirstname' => $this->userFirstname,
            'userSurname'=> $this->userSurname,
            'userMail' => $this->userMail,
        ];
    }

    public function getTask(){
        $task = new Task();
        $task->setSubject($this->subject);
        $task->setDescription($this->description);
        $task->setCategory($this->category);
        $task->setPriority($this->priority);
        $task->setComments($this->comments);
        $task->setIsEnable($this->isEnable);
        if($this->deadline){
            $deadline = new \DateTime($this->deadline);
            $task->setDeadline($deadline);
        }
        $task->setUserFirstname($this->userFirstname);
        $task->setUserSurname($this->userSurname);
        $task->setUserMail($this->userMail);
        return $task;
    }

}
