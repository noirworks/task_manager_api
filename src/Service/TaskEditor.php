<?php

namespace App\Service;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\TaskValidator;
use App\Service\TaskModel;

class TaskEditor{

    private $em;
    private $validator;

    public function __construct(EntityManagerInterface $em, TaskValidator $validator){
        $this->em = $em;
        $this->validator = $validator;
    }

    public function edit(Task $task, $params){
        if( count($params) > 0 ){
            foreach($params  as $key => $value ){
                switch($key){
                    case 'description':
                        $task->setDescription($value);
                        break;
                    case 'category':
                        $task->setCategory($value);
                        break;
                    case 'priority':
                        $task->setPriority($value);
                        break;
                    case 'comments':
                        $task->setComments($value);
                        break;
                    case 'isEnable':
                        $task->setIsEnable($value);
                        break;
                    case 'userFirstname':
                        $task->setUserFirstname($value);
                        break;
                    case 'userSurname':
                        $task->setUserSurname($value);
                        break;
                    case 'userMail':
                        $task->setUserMail($value);
                        break;
                }
            }
        }
        $this->em->flush();
        return $task;
    }

    public function enable(Task $task, $enable = true){
        $task->setIsEnable($enable);
        $this->em->flush();
        return $task;
    }

}
