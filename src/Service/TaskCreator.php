<?php

namespace App\Service;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\TaskValidator;
use App\Service\TaskModel;

class TaskCreator{

    private $em;
    private $validator;

    public function __construct(EntityManagerInterface $em, TaskValidator $validator){
        $this->em = $em;
        $this->validator = $validator;
    }

    public function create($userParams){
        $userParams['isEnable'] = false;
        $params = $this->validator->valid($userParams);
        $model = new TaskModel($params);
        $task = $model->getTask();
        $this->em->persist($task);
        $this->em->flush();
        return $task;
    }

}
