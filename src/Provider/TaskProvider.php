<?php

namespace App\Provider;

use App\Entity\Task;
use App\Service\TaskModel;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class TaskProvider
{

    private $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    public function save(Task $task){
        $this->taskRepository->save($task);
    }

    public function search(Array $params, $model = false): ?Array
    {
        $results = $this->taskRepository->search($params);
        if($model && count($results) ){
            $models = [];
            foreach($results as $task){
                $models[] = new TaskModel($task);
            }
            return $models;
        };
        return $results;
    }

    public function single(int $taskId, $model = false)
    {
        $task = $this->taskRepository->find($taskId);
        if($model){
            $model = new TaskModel($task);
            return $model;
        }
        return $task;
    }
}
